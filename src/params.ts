export interface Params {
  search: string;
  make: string;
  model: string;
  year: number;
  price: number;
  color: string;
}


export class Store implements Params {
  search: string;
  make: string;
  model: string;
  year: number;
  price: number;
  price_min: number;
  price_max: number;
  color: string;

  constructor(params: any) {
    this.search = params.search || null;
    this.make = params.make || null;
    this.model = params.model || null;
    this.year = params.year || null;
    this.price = params.price || null;
    this.price_min = params.price_min || null;
    this.price_max = params.price_max || null;
    this.color = params.color || null;
  }
}
