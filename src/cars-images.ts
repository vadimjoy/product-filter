export const auto =  {
  'Audi': 'https://kuruh.ru/foto/audi.jpg',
  'Chrysler': 'https://kuruh.ru/foto/chrysler.jpg',
  'Nissan': 'https://kuruh.ru/foto/nissan.jpg',
  'Jaguar': 'https://kuruh.ru/foto/jaguar.jpg',
  'Pontiac': 'https://kuruh.ru/foto/pontiac.jpg',
  'Chevrolet': 'https://kuruh.ru/foto/chevrolet.jpg',
  'Mazda': 'https://kuruh.ru/foto/mazda.jpg',
  'Mitsubishi': 'https://kuruh.ru/foto/mitsubishi.jpg',
  'Mercedes-Benz': 'https://kuruh.ru/foto/mercedes-benz.jpg',
  'Ferrari': 'https://kuruh.ru/foto/ferrari.jpg',
  'Jeep': 'https://kuruh.ru/foto/jeep.jpg',
  'BMW': 'https://kuruh.ru/foto/bmw.jpg',
  'Land Rover': 'https://kuruh.ru/foto/land%20rover.jpg',
  'Toyota': 'https://kuruh.ru/foto/toyota.jpg',
  'GMC': 'https://kuruh.ru/foto/gmc.jpg',
  'Buick': 'https://kuruh.ru/foto/buick.jpg',
  'Ford': 'https://kuruh.ru/foto/ford.jpg',
  'Plymouth': 'https://kuruh.ru/foto/plymouth.jpg',
  'Honda': 'https://kuruh.ru/foto/honda.jpg',
  'Porsche': 'https://kuruh.ru/foto/porsche.jpg',
  'MINI': 'https://kuruh.ru/foto/mini.jpg',
  'Dodge': 'https://kuruh.ru/foto/dodge.jpg',
  'Maybach': 'https://kuruh.ru/foto/maybach.jpg',
  'Mercury': 'https://kuruh.ru/foto/mercury.jpg',
  'Kia': 'https://kuruh.ru/foto/kia.jpg',
  'Hyundai': 'https://kuruh.ru/foto/hyundai.jpg',
  'Daewoo': 'https://kuruh.ru/foto/daewoo.jpg',
  'Lexus': 'https://kuruh.ru/foto/lexus.jpg',
  'Volkswagen': 'https://kuruh.ru/foto/volkswagen.jpg',
  'Volvo': 'https://kuruh.ru/foto/volvo.jpg',
  'Subaru': 'https://kuruh.ru/foto/subaru.jpg',
  'Lincoln': 'https://kuruh.ru/foto/lincoln.jpg',
  'Acura': 'https://kuruh.ru/foto/acura.jpg',
  'Lotus': 'https://kuruh.ru/foto/lotus.jpg',
  'Infiniti': 'https://kuruh.ru/foto/infiniti.jpg',
  'Cadillac': 'https://kuruh.ru/foto/cadillac.jpg',
  'Saturn': 'https://kuruh.ru/foto/saturn.jpg',
  'Holden': 'https://kuruh.ru/foto/holden.jpg',
  'Maserati': 'https://kuruh.ru/foto/maserati.jpg',
  'Scion': 'https://kuruh.ru/foto/scion.jpg',
  'Aston Martin': 'https://kuruh.ru/foto/aston%20martin.jpg',
  'Saab': 'https://kuruh.ru/foto/saab.jpg',
  'Suzuki': 'https://kuruh.ru/foto/suzuki.jpg',
  'Rolls-Royce': 'https://kuruh.ru/foto/rolls%20royce.jpg',
  'Isuzu': 'https://kuruh.ru/foto/isuzu.jpg',
  'Bentley': 'https://kuruh.ru/foto/bentley.jpg',
  'Eagle': 'https://kuruh.ru/foto/eagle.jpg',
  'Jensen': 'https://kuruh.ru/foto/jensen.jpg',
  'Tesla': 'https://kuruh.ru/foto/tesla.jpg',
  'Oldsmobile': 'https://kuruh.ru/foto/oldsmobile.jpg',
  'Lamborghini': 'https://kuruh.ru/foto/lamborghini.jpg',
  'Daihatsu': 'https://kuruh.ru/foto/daihatsu.jpg',
  'Spyker': 'https://kuruh.ru/foto/spyker.jpg',
};
