import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    search: null,
    make: null,
    model: null,
    year: null,
    price_min: null,
    price_max: null,
    color: null,
    catalog: null,
  },
  mutations: {
    SEARCH(state, newSearchString) {
      state.search = newSearchString;
    },
    MAKE(state, newMake) {
      state.make = newMake;
    },
    MODEL(state, newModel) {
      state.model = newModel;
    },
    YEAR(state, newYear) {
      state.year = newYear;
    },
    PRICE_MIN(state, newPrice) {
      state.price_min = newPrice;
    },
    PRICE_MAX(state, newPrice) {
      state.price_max = newPrice;
    },
    COLOR(state, newColor) {
      state.color = newColor;
    },
    CATALOG(store, data) {
      store.catalog = data;
    },
  },
  actions: {
    update({commit}, query) {
      commit('SEARCH', query.search);
      commit('MAKE', query.make);
      commit('MODEL', query.model);
      commit('YEAR', query.year);
      commit('PRICE_MIN', query.price_min);
      commit('PRICE_MAX', query.price_max);
      commit('COLOR', query.color);
    },
    updateCatalog({commit}, data) {
      commit('CATALOG', data)
    },
  }
})
