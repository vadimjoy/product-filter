module.exports = {
  chainWebpack: config => {
    config.module.rules.delete('svg');
  },
  configureWebpack: {
    module: {
      rules: [
        {
          test: /\.svg$/,
          loader: 'vue-svg-loader', // `vue-svg` for webpack 1.x
          options: {
            // optional [svgo](https://github.com/svg/svgo) options
            svgo: {
              plugins: [
                { removeAttrs: { attrs: '(fill|stroke)' } },
                { removeViewBox: false },
                { removeDimensions: true },
                { removeDoctype: true },
                { removeComments: true }
              ]
            }
          }
        }
      ]
    }
  }
};
